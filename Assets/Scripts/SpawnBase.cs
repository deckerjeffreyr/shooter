using UnityEngine;
using System.Collections;

public abstract class SpawnBase : MonoBehaviour {

	public GameObject [] resources;
	public float [] spawnRatios;
	public float enemySpeed = 1.0f;
	public bool invert = false;

	// Potential Spawn Point Bounds
	protected float verticalMax;
	protected float horizontalMax;
	protected float minX;
	protected float maxX;
	protected float minY;
	protected float maxY;

	private float maxRandom = 0f;

	void Start () {
		CalculateBounds ();
		CalculateMaxRandom ();
	}

	public virtual void CalculateBounds () {
		verticalMax = Camera.main.orthographicSize;
		horizontalMax = verticalMax * Screen.width / Screen.height;
		// Get the spawn point ranges
		minX = -horizontalMax;
		maxX = horizontalMax;
		minY = -verticalMax;
		maxY = verticalMax;
	}

	public void SpawnObject () {
		Vector3 spawnPoint = GetSpawnOrigin ();
		Vector3 targetPoint = GetSpawnTarget (spawnPoint);
		Vector2 velocity = (targetPoint - spawnPoint).normalized * enemySpeed;

		if (invert) {
			spawnPoint *= -1;
			targetPoint *= -1;
			velocity *= -1;
		}

		GameObject res = GetRandomRes ();

		Debug.Log (string.Format ("Velocity: {0}", velocity));

		if (res == null) {
			Debug.LogError ("Null Res On Spawn");
			return;
		}

		GameObject go = Instantiate (res, spawnPoint, Quaternion.identity) as GameObject;
		go.GetComponent<Rigidbody2D> ().velocity = velocity;

		OnObjectSpawned (go);
	}

	protected virtual void OnObjectSpawned (GameObject gameObject) {}

	protected abstract Vector3 GetSpawnOrigin ();

	protected abstract Vector3 GetSpawnTarget (Vector3 origin);

	private GameObject GetRandomRes () {
		float random = Random.Range (0f, maxRandom);
		float total = 0f;

		for (int i = 0; i < spawnRatios.Length; i++) {
			total += spawnRatios [i];
			if (total >= random) {
				return resources [i];
			}
		}

		Debug.LogError (string.Format ("Null Res On Spawn For Ratio: {0} in {1}", random, total));
		return null;
	}

	private void CalculateMaxRandom () {
		foreach (float ratio in spawnRatios) {
			maxRandom += ratio;
		}
	}
}
