using UnityEngine;
using System.Collections;

public class SpiralShot : PowerUp {
	
	public Gun gun;
	private PlayerController player;

	public override void Activate (PlayerController player) {
		this.player = player;
		gun.SetPlayer (player);
		player.SetWeaponEnabled (false);
		StartCoroutine ("ShootSpiral");
	}

	IEnumerator ShootSpiral () {
		float totalRotation = 0f;

		while (totalRotation < 360) {
			player.transform.rotation *= Quaternion.Euler (0, 0, -10);
			gun.Shoot (player.transform.rotation);
			totalRotation += 10;
			yield return new WaitForSeconds (.01f);
		}

		player.SetWeaponEnabled (true);
		Destroy (gameObject);
	}
}