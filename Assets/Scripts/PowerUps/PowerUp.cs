using UnityEngine;
using System.Collections;

public abstract class PowerUp : MonoBehaviour {
	public abstract void Activate (PlayerController player);
}
