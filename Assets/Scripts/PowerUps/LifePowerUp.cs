using UnityEngine;
using System.Collections;

public class LifePowerUp : PowerUp {

	public int lifeGain = 5;

	public override void Activate (PlayerController player) {
		player.lifeController.AddLives (lifeGain);
	}
}
