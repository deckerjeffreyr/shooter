using UnityEngine;
using System.Collections;

public class ShockWave : PowerUp {
	public override void Activate (PlayerController player) {
		StartCoroutine ("Shock");
	}

	IEnumerator Shock () {
		float verticalSize = Camera.main.orthographicSize;
		float horizontalSize = verticalSize * Screen.width / Screen.height;
		float finalRadius = Mathf.Sqrt ((verticalSize * verticalSize) + (horizontalSize * horizontalSize));

		for (float i = 1f; i < finalRadius; i += 1f) {
			Collider2D [] collisions = Physics2D.OverlapCircleAll (transform.position, i);
			foreach (Collider2D collision in collisions) {
				if (collision.gameObject.CompareTag ("Asteroid")) {
					Destroy (collision.gameObject);
				}
			}
			yield return new WaitForSeconds (.1f);
		}

		Destroy (gameObject);
	}
}
