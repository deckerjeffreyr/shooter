using UnityEngine;
using System.Collections;

public class Shield : PowerUp {

	public override void Activate (PlayerController player) {
		player.lifeController.AddShield ();
	}
}
