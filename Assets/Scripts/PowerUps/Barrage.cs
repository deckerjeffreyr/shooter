using UnityEngine;
using System.Collections;

public class Barrage : PowerUp {

	public GameObject projectileRes;
	public float projectileVelocity = 10f;

	private int volleys = 3;
	private int volleySize = 35;
	private float volleyGap = .5f;
	private float degreeChange = 5f;

	public override void Activate (PlayerController player) {
		StartCoroutine (PerformBarage (player));
	}

	private IEnumerator PerformBarage (PlayerController player) {
		for (int i = 0; i < volleys; i++) {
			for (int j = 1; j <= volleySize; j++) {
				GameObject projectile = Instantiate (projectileRes, player.transform.position, Quaternion.Euler (0, 0, -degreeChange * j));
				ProjectileController projectileController = projectile.GetComponent<ProjectileController> ();
				projectileController.SetShooter (player);
				projectile.GetComponent<Rigidbody2D> ().velocity = projectile.transform.right * projectileVelocity;
			}
			yield return new WaitForSeconds (volleyGap);
		}
		Destroy (gameObject);
		yield return null;
	}
}
