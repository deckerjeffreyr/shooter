﻿using UnityEngine;

public class TriggerSplit : MonoBehaviour {
	
	public int childrenCount;
	public GameObject childResource;

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Projectile") && gameObject.GetComponent<SpriteRenderer> ().isVisible) {
			SpawnChildren ();
		} else if (collision.gameObject.CompareTag ("Player")) {
			Destroy (gameObject);
		}
	}

	private void SpawnChildren () {
		if (childResource != null) {
			for (int i = 0; i < childrenCount; i++) {
				Vector2 velocity = new Vector2 (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f)).normalized * 3;
				GameObject instance = Instantiate (childResource, transform.position, Quaternion.identity) as GameObject;
				instance.GetComponent<Rigidbody2D> ().velocity = velocity;
			}
		}
		Destroy (gameObject);
	}
}
