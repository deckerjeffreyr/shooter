using UnityEngine;

public class VisibilityDestructor : MonoBehaviour {
	
	private float verticalMax;
	private float horizontalMax;

	void Start () {
		verticalMax = Camera.main.orthographicSize;
		horizontalMax = verticalMax * Screen.width / Screen.height;
	}

	void Update () {
		if (Mathf.Abs (transform.position.x) > horizontalMax * 2
			|| Mathf.Abs (transform.position.y) > verticalMax * 2) {
			Destroy (gameObject);
		}
	}

	void OnBecameInvisible () {
		Destroy (gameObject);
	}
}
