using UnityEngine;

public class LifeCountdown : MonoBehaviour {

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Asteroid")){
			GameController.Instance.ApplyDamage (collision.gameObject.GetComponent<EnemyController> ().damage);	
		}
	}
}
