using UnityEngine;

public class ProjectileHitDestructor : MonoBehaviour {

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Projectile")) {
			Destroy (gameObject);
		}
	}
}