using UnityEngine;
using System.Collections;

public class TriggerReduce : MonoBehaviour {

	public GameObject childResource;

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Projectile") && gameObject.GetComponent<SpriteRenderer> ().isVisible) {
			GameController.Instance.AddPoint ();
			SpawnChildren (); // TODO: Instead of spawning children, let's tell the object we've been hit and let it handle the rest (sprite update, life update, etc.)
		} else if (collision.gameObject.CompareTag ("Player")) {
			Destroy (gameObject);
		}
	}

	private void SpawnChildren () {
		if (childResource != null) {
			GameObject instance = Instantiate (childResource, transform.position, Quaternion.identity) as GameObject;
			instance.GetComponent<Rigidbody2D> ().velocity = GetComponent<Rigidbody2D> ().velocity;
		}
		Destroy (gameObject);
	}
}
