using UnityEngine;

public class EnemyDestructor : MonoBehaviour {

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Projectile") && gameObject.GetComponent<SpriteRenderer> ().isVisible) {
			GameController.Instance.AddPoint ();
			Destroy (gameObject);
		} else if (collision.gameObject.CompareTag ("Player")) {
			Destroy (gameObject);
		}
	}
}
