﻿using UnityEngine;

public class ProjectileController : MonoBehaviour {

	private PlayerController shooter;

	public void SetShooter (PlayerController shooter) {
		this.shooter = shooter;
	}

	public PlayerController GetShooter () {
		return shooter;
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Asteroid") 
		    || collision.gameObject.CompareTag ("Collectable")) {
			// TODO - AcuracyController.RegisterHit()
			Destroy (gameObject);
		}
	}
}
