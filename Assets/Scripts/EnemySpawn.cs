﻿using UnityEngine;

public class EnemySpawn : MonoBehaviour {

	public OrientationChangeMonitor orientationChangeMonitor;
	public ScoreController scoreController;

	public float startingCadence = .75f;
	public float finalCadence = .5f;

	public SpawnBase [] spawns;
	public float [] spawnRatios;

	private float maxRandom = 0f;

	// Use this for initialization
	void Start () {
		CalculateMaxRandom ();
		ScheduleSpawn ();

		foreach (SpawnBase spawn in spawns) {
			orientationChangeMonitor.OnOrientationChange.AddListener (spawn.CalculateBounds);
		}
	}

	private void OnDestroy () {
		foreach (SpawnBase spawn in spawns) {
			orientationChangeMonitor.OnOrientationChange.RemoveListener (spawn.CalculateBounds);
		}
	}

	private void ScheduleSpawn () {
		float timeTilNextSpawn = Mathf.Max (startingCadence - (scoreController.GetScore () / 400f), .5f) ;
		Invoke ("Spawn", timeTilNextSpawn);
	}

	private SpawnBase GetRandomSpawn () {
		float random = Random.Range (0f, maxRandom);
		float total = 0f;

		for (int i = 0; i < spawnRatios.Length; i++) {
			total += spawnRatios [i];
			if (total >= random) {
				return spawns [i];
			}
		}

		return null;
	}

	private void Spawn () {
		GetRandomSpawn ().SpawnObject ();
		ScheduleSpawn ();
	}

	private void CalculateMaxRandom () {
		foreach (float ratio in spawnRatios) {
			maxRandom += ratio;
		}
	}
}
