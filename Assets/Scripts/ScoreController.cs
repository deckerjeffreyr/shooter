using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	public Text scoreText;

	private long score = 0;

	private void Start () {
		UpdateScoreText ();
	}

	public void AddPoint () {
		score++;
		UpdateScoreText ();
	}

	public void AddPoints (long points) {
		score += points;
		UpdateScoreText ();
	}

	public long GetScore () {
		return score;
	}

	private void UpdateScoreText () {
		scoreText.text = "Score: " + score;
	}
}
