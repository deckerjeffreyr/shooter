using UnityEngine;
using System.Collections.Generic;

public class MeteorSpawn : SpawnBase {

	protected override Vector3 GetSpawnOrigin () {
		float xSpawn, ySpawn;

		xSpawn = Random.Range (minX, maxX);
		ySpawn = maxY + 1;

		return new Vector3 (xSpawn, ySpawn);
	}

	protected override Vector3 GetSpawnTarget (Vector3 origin) {
		float xTarget, yTarget;

		xTarget = Random.Range (minX, maxX);
		yTarget = -origin.y;

		return new Vector3 (xTarget, yTarget);
	}
}
