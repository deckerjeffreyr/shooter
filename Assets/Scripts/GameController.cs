﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private static GameController instance;

	public static GameController Instance {
		get {
			if (instance == null) {
				throw new UnassignedReferenceException ();
			}
			return instance;
		}
	}

	public UIController uiController;
	public EnemySpawn enemySpawn;
	public ScoreController scoreController;
	public LifeController lifeController;

	public void Awake () {
		instance = this;
	}

	public void AddLife () {
		lifeController.AddLife ();
	}

	public void ApplyDamage (int damage) {
		lifeController.ApplyDamage (damage);

		if (!lifeController.HasLife ()) {
			HandleGameOver ();
		}
	}

	public void AddPoint () {
		scoreController.AddPoint ();
	}

	// Scene Management
	public void Restart () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void ExitToMain () {
		SceneManager.LoadScene ("Menu");
	}

	// Lifecycle Management
	public void Pause () {
		Time.timeScale = 0;
		uiController.HandlePause ();
	}

	public void Resume () {
		Time.timeScale = 1;
		uiController.HandleResume ();
	}

	private void HandleGameOver () {
		// TODO - Save high score
		uiController.HandleGameOver ();
	}

	public void OnApplicationPause (bool pause) {
		if (pause) {
			Pause ();
		}
	}
}
