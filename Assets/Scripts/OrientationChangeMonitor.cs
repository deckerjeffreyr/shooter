using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class OrientationChangeMonitor : MonoBehaviour {
	public enum Orientations { Landscape, Portrait };

	public UnityEvent OnOrientationChange = new UnityEvent ();

	public float minOrthographicSize = 6.0f;
	public float currentAspectRatio;
	public Orientations currentOrientation = Orientations.Landscape;

	// Use this for initialization
	void Start () {
		currentAspectRatio = (float)Camera.main.pixelWidth / (float)Camera.main.pixelHeight;
		currentOrientation = currentAspectRatio >= 1 ? Orientations.Landscape : Orientations.Portrait;
		Camera.main.orthographicSize = currentOrientation == Orientations.Landscape ? minOrthographicSize : minOrthographicSize / currentAspectRatio;
		OnOrientationChange.Invoke ();
	}

	// Update is called once per frame
	void Update () {
		float aspectRatio = (float)Camera.main.pixelWidth / (float)Camera.main.pixelHeight;
		Orientations orientation = aspectRatio >= 1 ? Orientations.Landscape : Orientations.Portrait;

		if (orientation != currentOrientation) {
			currentAspectRatio = aspectRatio;
			currentOrientation = orientation;
			Camera.main.orthographicSize = currentOrientation == Orientations.Landscape ? minOrthographicSize : minOrthographicSize / currentAspectRatio;
			OnOrientationChange.Invoke ();
		}
	}
}
