using UnityEngine;
using System.Collections;

public class DropShipController : MonoBehaviour {

	public GameObject bombRes;
	public float dropLocation = 0;
	public float dropCount = 3;

	private bool hasDropped = false;

	// Update is called once per frame
	void Update () {
		if (Mathf.Abs (transform.position.x - dropLocation) <= .05f && !hasDropped) {
			StartCoroutine (DropBombs ());
		}
	}

	IEnumerator DropBombs () {
		hasDropped = true;
		Rigidbody2D rigidBody = GetComponent<Rigidbody2D> ();
		Vector2 originalVelocity = rigidBody.velocity;
		rigidBody.velocity = new Vector2 ();
		yield return new WaitForSeconds (2f);
		for (int i = 0; i < dropCount; i++) {
			GameObject projectile = Instantiate (bombRes, transform.position, Quaternion.identity);
			projectile.GetComponent<Rigidbody2D> ().velocity = transform.up * 2f;
			yield return new WaitForSeconds (.5f);
		}
		rigidBody.velocity = originalVelocity;
	}

	public void RandomizeDropLocation (float min, float max) {
		dropLocation = Random.Range (min, max);
	}
}
