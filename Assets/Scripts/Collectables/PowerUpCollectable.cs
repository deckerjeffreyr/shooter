using UnityEngine;

public class PowerUpCollectable : Collectable {
	
	public override void OnItemCollected (PlayerController player) {
		GameObject powerUpCollectable = Instantiate (collectableItem) as GameObject;
		PowerUp powerUp = powerUpCollectable.GetComponent<PowerUp> ();
		player.AttachPowerUp (powerUp);	
	}
}
