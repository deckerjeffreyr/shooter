using UnityEngine;
using System.Collections;

public abstract class Collectable : MonoBehaviour {

	public GameObject collectableItem;
	public int timeAlive = 5;

	public abstract void OnItemCollected (PlayerController player);

	private void Start () {
		StartCoroutine ("Timeout");
	}

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.CompareTag ("Projectile")) {
			ProjectileController projectile = collision.gameObject.GetComponent<ProjectileController> ();
			if (projectile != null) {
				OnItemCollected (projectile.GetShooter ());
			}
			Destroy (gameObject);
		}
	}

	// TODO - Make timeout a separate script
	IEnumerator Timeout () {
		yield return new WaitForSeconds (timeAlive);
		Destroy (gameObject);
		yield return "timed out";
	}
}
