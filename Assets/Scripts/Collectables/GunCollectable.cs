using UnityEngine;

public class GunCollectable : Collectable {
	
	public override void OnItemCollected (PlayerController player) {
		GameObject gunCollectable = Instantiate (collectableItem) as GameObject;
		Gun gun = gunCollectable.GetComponent<Gun> ();
		player.AttachGun (gun);
	}
}
