﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public Canvas hud;
	public Canvas pauseMenu;
	public Canvas gameOverMenu;

	public Button pauseButton;

	// Use this for initialization
	void Start () {
		HideGameOverMenu ();
		HidePauseMenu ();
		ShowHud ();
	}

	public void HandlePause () {
		HidePauseButton ();
		ShowPauseMenu ();
	}

	public void HandleResume () {
		HidePauseMenu ();
		ShowPauseButton ();
	}

	public void HandleGameOver () {
		HidePauseButton ();
		ShowGameOverMenu ();
	}

	public void ShowGameOverMenu () {
		gameOverMenu.gameObject.SetActive (true);
	}

	public void HideGameOverMenu () {
		gameOverMenu.gameObject.SetActive (false);
	}

	public void ShowPauseMenu () {
		pauseMenu.gameObject.SetActive (true);
	}

	public void HidePauseMenu () {
		pauseMenu.gameObject.SetActive (false);
	}

	public void ShowPauseButton () {
		pauseButton.gameObject.SetActive (true);
	}

	public void HidePauseButton() {
		pauseButton.gameObject.SetActive (false);
	}

	public void ShowHud () {
		hud.gameObject.SetActive (true);
	}

	public void HideHud () {
		hud.gameObject.SetActive (false);
	}
}
