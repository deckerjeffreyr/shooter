using UnityEngine;
using System.Collections;

public class TripleShooter : SingleShooter {

	private int shotsRemaining = 25;

	override public void Shoot (Quaternion rotation) {
		Quaternion leftRotation = rotation * Quaternion.Euler (0, 0, 10);
		Quaternion rightRotation = rotation * Quaternion.Euler (0, 0, -10);

		// TODO - This might hurt us when it comes to recording accuracy
		// Maybe move single shooters shoot impl into a static utility 
		base.Shoot (rotation);
		base.Shoot (leftRotation);
		base.Shoot (rightRotation);

		shotsRemaining--;
		// TODO - Update UI Element

		if (shotsRemaining == 0) {
			player.AttachOriginalGun ();
			Destroy (gameObject);
		}
	}
}
