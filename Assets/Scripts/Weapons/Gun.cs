using UnityEngine;

public abstract class Gun : MonoBehaviour {

	public float velocity = 10f;
	public Vector3 projectileOrigin = new Vector3 (0, 0);
	public GameObject projectileRes;
	protected PlayerController player;

	public void SetPlayer (PlayerController player) {
		this.player = player;
	}

	public abstract void Shoot (Quaternion rotation);
}
