using UnityEngine;

public class SingleShooter : Gun {

	override public void Shoot (Quaternion rotation) {
		GameObject projectile = Instantiate (projectileRes, transform.position, rotation);
		ProjectileController projectileController = projectile.GetComponent<ProjectileController> ();
		projectileController.SetShooter (player);
		projectile.GetComponent<Rigidbody2D> ().velocity = projectile.transform.right * velocity;	
	}
}
