﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	public LifeController lifeController;

	private Gun originalGun;
	private Gun activeGun;

	private bool weaponEnabled = true;

	void Start () {
		originalGun = Instantiate(Resources.Load ("prefabs/SingleShooter") as GameObject, transform.position, transform.rotation).GetComponent<Gun> ();
		AttachGun (originalGun);
	}

	void Update () {
		if (weaponEnabled) {
			if (Input.GetMouseButtonDown (0) && GUI.GetNameOfFocusedControl () == "") {
				RotateAndShoot ();
			}
		}
	}

	private void RotateAndShoot () {
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Vector3 target = mousePosition;
		Vector3 difference = target - transform.position;
		difference.Normalize ();
		Quaternion rotation = Quaternion.Euler (0f, 0f, Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg);
		// TODO - Add this to rotation if we want to account for origin offset
		//float yForAtan = Vector3.Distance(mountPoint.transform.position, transform.position);//mountPoint.transform.position.y - transform.position.y;//distanceFromGunToPlayer;
		//float xForAtan = Vector3.Distance(transform.position, mousePosition);//transform.position.x - mousePosition.x;//distanceFromPlayerToCursor;
		//float alpha = Mathf.Atan2(yForAtan, xForAtan) * Mathf.Rad2Deg;
		transform.rotation = rotation;
		activeGun.Shoot (transform.rotation);
	}

	public void AttachGun (Gun gun) {
		gun.gameObject.transform.parent = transform;
		gun.gameObject.transform.position = transform.position;
		gun.SetPlayer (this);
		activeGun = gun;
	}

	public void AttachOriginalGun () {
		if (activeGun == originalGun) {
			return;
		}

		Gun currentGun = activeGun;
		activeGun = originalGun;
		Destroy (currentGun.gameObject);

	}

	public void AttachPowerUp (PowerUp powerUp) {
		powerUp.transform.parent = transform;
		powerUp.Activate (this);
	}

	public void SetWeaponEnabled (bool enabled) {
		weaponEnabled = enabled;	
	}
}
