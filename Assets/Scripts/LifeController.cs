using UnityEngine;
using UnityEngine.UI;

public class LifeController: MonoBehaviour {
	
	public Image lifeIndicator;
	public Image shieldIndicator;
	public Image shieldBar;

	public long maxLife = 10;
	public long baseLife = 10;
	private long life;

	public long maxShield = 5;
	public long baseShield = 0;
	private long shield;

	private void Start () {
		life = baseLife;
		shield = baseShield;
		UpdateLifeIndicator ();
		UpdateShieldIndicator ();
	}

	public void ApplyDamage (int damage) {
		if (HasShield ()) {
			DamageShield (damage);
			return;
		}
		life -= damage;
		life = (long)Mathf.Max (0, life);
		UpdateLifeIndicator ();
	}

	public void AddLife () {
		AddLives (1);
	}

	public void AddLives(int lives) {
		life += lives;
		UpdateLifeIndicator ();
	}

	public bool HasLife () {
		return life > 0;
	}

	private void UpdateLifeIndicator () {
		lifeIndicator.fillAmount = (float)life/(float)maxLife;
	}

	public void AddShield () {
		shield = maxShield;
		UpdateShieldIndicator ();
	}

	private void DamageShield (int damage) {
		shield -= damage;
		shield = (long) Mathf.Max (0, shield);
		UpdateShieldIndicator ();
	}

	private bool HasShield () {
		return shield > 0;
	}

	private void UpdateShieldIndicator () {
		shieldBar.gameObject.SetActive (HasShield ());
		shieldIndicator.fillAmount = (float)shield / (float)maxShield;
	}
}
