using UnityEngine;
using System.Collections;

public class ShipSpawn : SpawnBase {

	protected override void OnObjectSpawned (GameObject gameObject) {
		gameObject.GetComponent<DropShipController> ().RandomizeDropLocation (minX + 1, maxX - 1);	
	}

	protected override Vector3 GetSpawnOrigin () {
		float xSpawn, ySpawn;

		bool usePositiveValue = Random.value > 0.5f;

		xSpawn = usePositiveValue ? maxX + 1 : minX - 1;
		ySpawn = Random.Range (minY, maxY - 1);

		return new Vector3 (xSpawn, ySpawn);
	}

	protected override Vector3 GetSpawnTarget (Vector3 origin) {
		return new Vector3 (origin.x * -1, origin.y);
	}

	public override void CalculateBounds () {
		verticalMax = Camera.main.orthographicSize;
		horizontalMax = verticalMax * Screen.width / Screen.height;
		// Get the spawn point ranges
		minX = -horizontalMax;
		maxX = horizontalMax;
		minY = 0;
		maxY = verticalMax;
	}
}
